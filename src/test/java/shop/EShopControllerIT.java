package shop;

import org.junit.jupiter.api.*;
import storage.NoItemInStorage;
import storage.Storage;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.*;

class EShopControllerIT {
    private Item[] storageItems;
    private String testName;
    private String testAdress;

    @BeforeEach
    public void setUp()
    {
        this.testName = "Libuse Vitonova";
        this.testAdress = "V lese 81,Hrdliv 265 05";
        EShopController.startEShop();
        this.storageItems = new Item[]{
                new StandardItem(1, "Dancing Panda v.2", 5000, "GADGETS", 5),
                new StandardItem(2, "Dancing Panda v.3 with USB port", 6000, "GADGETS", 10),
                new StandardItem(3, "Screwdriver", 200, "TOOLS", 5),
                new DiscountedItem(4, "Star Wars Jedi buzzer", 500, "GADGETS", 30, "1.8.2013", "1.12.2013"),
                new DiscountedItem(5, "Angry bird cup", 300, "GADGETS", 20, "1.9.2013", "1.12.2013"),
                new DiscountedItem(6, "Soft toy Angry bird (size 40cm)", 800, "GADGETS", 10, "1.8.2013", "1.12.2013")
        };
    }
    @AfterEach
    public void breakDown()
    {
        EShopController.setArchive(null);
        EShopController.setCarts(null);
        EShopController.setStorage(null);
    }
    @Test
    @DisplayName("Testing normal function of cart")
    public void validShopingTestFromStartToEnd()
    {

        Storage testStorage = EShopController.getStorage();
        for (Item item: storageItems)
        {
            testStorage.insertItems(item,10);
        }
        EShopController.newCart();

        ShoppingCart testCart = EShopController.getCarts().get(0);

        for (Item item: storageItems)
        {
            testCart.addItem(item);
        }
        assertEquals(storageItems.length,testCart.items.size());
        try {
            EShopController.purchaseShoppingCart(testCart,testName,testAdress);
        } catch (NoItemInStorage noItemInStorage) {
            noItemInStorage.printStackTrace();
        }
        for (Item item: storageItems)
        {
           assertEquals(9,testStorage.getItemCount(item.getID()));
        }
        for (Item item: storageItems)
        {
            assertEquals(1,EShopController.getArchive().getHowManyTimesHasBeenItemSold(item));
        }
    }
    @Test
    @DisplayName("Testing empty")
    public void fillAndEmptyShopingCartAndPurchase()
    {
        Storage testStorage = EShopController.getStorage();
        for (Item item: storageItems)
        {
            testStorage.insertItems(item,10);
        }
        EShopController.newCart();

        ShoppingCart testCart = EShopController.getCarts().get(0);

        for (Item item: storageItems)
        {
            testCart.addItem(item);
        }
        for (Item item: storageItems)
        {
            testCart.removeItem(item.getID());
        }
        assertEquals(0,testCart.items.size());
        try {
            EShopController.purchaseShoppingCart(testCart,testName,testAdress);
        } catch (NoItemInStorage noItemInStorage) {
            noItemInStorage.printStackTrace();
        }
        for (Item item: storageItems)
        {
            assertEquals(10,testStorage.getItemCount(item.getID()));
        }
        for (Item item: storageItems)
        {
            assertEquals(0,EShopController.getArchive().getHowManyTimesHasBeenItemSold(item));
        }
    }
    @Test
    @DisplayName("Throws asserstion Buying more than that is in storage")
    public void tryBuyMOreThanIsInStorage()
    {

        Storage testStorage = EShopController.getStorage();
        for (Item item: storageItems)
        {
            testStorage.insertItems(item,1);
        }
        EShopController.newCart();

        ShoppingCart testCart = EShopController.getCarts().get(0);

        testCart.addItem(storageItems[0]);
        testCart.addItem(storageItems[0]);
        assertEquals(2,testCart.items.size());
        Assertions.assertThrows(Exception.class,()->EShopController.purchaseShoppingCart(testCart,testName,testAdress));
    }

}