package shop;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.*;

class StandardItemTest {

    @Test
    @DisplayName("Test konstruktoru")
    @Order(1)
    public void testConstructor()
    {
        //arrange
        int id = 10;
        String name = "testItem";
        float price = 345.67f;
        String categrory = "Zbozi";
        int loyaltyPoints = 34;
        //act
        StandardItem testStandardItem = new StandardItem(id,name,price,categrory,loyaltyPoints);
        assertEquals(id,testStandardItem.getID());
        assertEquals(name,testStandardItem.getName());
        assertEquals(price,testStandardItem.getPrice());
        assertEquals(categrory,testStandardItem.getCategory());//vice assertu v jedne tride jsem si dovolil proto, ze pokud jeden neprojde, znamena to
                                                                // ze je konstruktor spatne
        //assert


    }
    @DisplayName("Test kopirovani objektu")
    @Test
    @Order(3)
    public void testCopy()
    {
        int id = 10;
        String name = "testItem";
        float price = 345.67f;
        String categrory = "Zbozi";
        int loyaltyPoints = 34;
        //act
        StandardItem testOriginal = new StandardItem(id,name,price,categrory,loyaltyPoints);
        StandardItem testCopy = testOriginal.copy();
        assertEquals(testOriginal.getID(),testCopy.getID());
        assertEquals(testOriginal.getName(),testCopy.getName());
        assertEquals(testOriginal.getPrice(),testCopy.getPrice());
        assertEquals(testOriginal.getCategory(),testCopy.getCategory());

    }
    @ParameterizedTest(name = "Test from csv should be {10}")
    @CsvSource({"12,testcsv,34.5f,category,29,12,testcsv,34.5f,category,29,true",
            "12,jiny name,34.5f,category,29,12,testcsv,34.5f,category,29,false",
            "12,testcsv,34.5f,category,29,12,testcsv,34.5f,category,35,false"
    })
    @Order(2)
    public void testEquals(int firstId,String firstName,float firstPrice,String firstCategory,int firstLoyaltyPoints
            ,int secondId,String secondName, float secondPrice,String secondCategory,int secondLoyaltyPoints,boolean correct)
    {
        StandardItem first = new StandardItem(firstId,firstName,firstPrice,firstCategory,firstLoyaltyPoints);
        StandardItem second = new StandardItem(secondId,secondName,secondPrice,secondCategory,secondLoyaltyPoints);
        assertEquals(correct,first.equals(second));
    }



}