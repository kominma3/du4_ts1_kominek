package shop;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.internal.matchers.Null;

import static org.junit.jupiter.api.Assertions.*;

class OrderTest {
    @Test
    @DisplayName("Right constructor function test")
    public void constructorTestNormal()
    {
        //arrange
        ShoppingCart cart = new ShoppingCart();
        String customerName = "Libuse Vitonova";
        String customerAdress = "Veletržní ů§";
        int state = 4;
        Order order = new Order(cart,customerName,customerAdress,state);
        assertEquals(cart.getCartItems(),order.getItems());
        assertEquals(customerName,order.getCustomerName());
        assertEquals(state,order.getState());
    }
    @Test
    @DisplayName("null pointer test")
    public void constructorTestNull()
    {
        assertThrows(NullPointerException.class,()-> {
            ShoppingCart cart = null;
            String customerName = "Marek Peckaů+;";
            String customerAdress = "Na přá7t.k, u Mostu ěšř 23";
            int state = 4;
            Order order = new Order(cart, customerName, customerAdress, state);
            assertEquals(cart.getCartItems(), order.getItems());
            assertEquals(customerName, order.getCustomerName());
            assertEquals(state, order.getState());
        });
    }
    @Test
    @DisplayName("Second constructor test")
    public void constructorTestWithoutState()
    {
        //arrange
        ShoppingCart cart = new ShoppingCart();
        String customerName = "Emilie Malá";
        String customerAdress = "Třebichovice 34, 345 01";
        Order order = new Order(cart,customerName,customerAdress);
        assertEquals(cart.getCartItems(),order.getItems());
        assertEquals(customerName,order.getCustomerName());
        assertEquals(0,order.getState());
    }

}