package archive;

import org.junit.jupiter.api.*;
import shop.Item;
import shop.Order;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;


import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

import org.mockito.Mockito;
import shop.ShoppingCart;
import shop.StandardItem;

class PurchasesArchiveTest {
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;
    private PurchasesArchive purchaseArchive;
    @BeforeEach
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
    }
    @AfterEach
    public void restoreStreams() {
        System.setOut(originalOut);
    }
    @Test
    void printItemPurchaseStatisticsEmptyTest() {
        this.purchaseArchive = new PurchasesArchive();
        purchaseArchive.printItemPurchaseStatistics();
        assertEquals("ITEM PURCHASE STATISTICS:\r\n",outContent.toString());
    }

    @Test
    @DisplayName("list contains item")
    void getHowManyTimesHasBeenItemSoldTestContains() {
        HashMap mockHash = mock(HashMap.class);
        ArrayList<Order> orders = mock(ArrayList.class);
        ItemPurchaseArchiveEntry mockArchive = mock(ItemPurchaseArchiveEntry.class);
        when(mockArchive.getCountHowManyTimesHasBeenSold()).thenReturn(11);
        this.purchaseArchive = new PurchasesArchive(mockHash,orders);
        when(mockHash.containsKey(anyInt())).thenReturn(true);
        when(mockHash.get(anyInt())).thenReturn(mockArchive);
        Item mockItem = mock(StandardItem.class);
        when(mockItem.getID()).thenReturn(20);

        int ret = purchaseArchive.getHowManyTimesHasBeenItemSold(mockItem);

        Assertions.assertEquals(11,ret);
    }
    @Test
    @DisplayName("list doesn contains item")
    void getHowManyTimesHasBeenItemSoldTestNotContains() {
        HashMap mockHash = mock(HashMap.class);
        ArrayList<Order> orders = mock(ArrayList.class);
        this.purchaseArchive = new PurchasesArchive(mockHash,orders);
        when(mockHash.containsKey(anyInt())).thenReturn(false);
        Item mockItem = mock(StandardItem.class);

        int ret = purchaseArchive.getHowManyTimesHasBeenItemSold(mockItem);

        Assertions.assertEquals(0,ret);
    }


    @Test
    @DisplayName("check if it was added to array")
    void putOrderToPurchasesArchiveTestAddNoneExistingArray() {
        Order order = new Order(new ShoppingCart(),"Jake Peralta","Brooklyn 99");
        HashMap mockHashMap = mock(HashMap.class);
        ArrayList<Order> orders = mock(ArrayList.class);
        when(orders.get(anyInt())).thenReturn(order);
        this.purchaseArchive = new PurchasesArchive(mockHashMap,orders);

        purchaseArchive.putOrderToPurchasesArchive(order);


        assertEquals(order,orders.get(10));

    }
    @Test
    @DisplayName("Test archive")
    void putOrderToPurchasesArchiveTestAddIntoArchive() {
        ArrayList<Item> items = new ArrayList<>();
        Item testItem = new StandardItem(12,"Topinkovac",34.5f,"Spotrebice",34);
        items.add(testItem);
        Order order = new Order(new ShoppingCart(items),"Libuse Vitonova","U panku 35, Kladno 645 03");
        ItemPurchaseArchiveEntry testArchive = new ItemPurchaseArchiveEntry(testItem);

        HashMap<Integer,ItemPurchaseArchiveEntry> mockHashMap = mock(HashMap.class);
        ArrayList<Order> orders = mock(ArrayList.class);

        when(mockHashMap.containsKey(anyInt())).thenReturn(true);
        when(mockHashMap.get(anyInt())).thenReturn(testArchive);
        this.purchaseArchive = new PurchasesArchive(mockHashMap,orders);
        purchaseArchive.putOrderToPurchasesArchive(order);
        verify(mockHashMap).containsKey(anyInt());
        assertEquals(2,testArchive.getCountHowManyTimesHasBeenSold());

    }

}