package storage;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import shop.Item;
import shop.StandardItem;

import static org.junit.jupiter.api.Assertions.*;

class ItemStockTest {
    @Test
    @DisplayName("normal test of constructor")
    public void constructorTest()
    {
        Item item = new StandardItem(12,"Varná konvice",47.67f,"Spotrebic",34);
        ItemStock itemStock = new ItemStock(item);
        assertEquals(item,itemStock.getItem());
    }
    @ParameterizedTest(name = "Parametrizied test add {0} to count should equal {1}")
    @CsvSource({"12,12","-3,-3","0,0"})
    public void itemAddCountTest(int addX,int result)
    {
        Item item = new StandardItem(12,"žehlička",47.67f,"Spotrebic",34);
        ItemStock itemStock = new ItemStock(item);
        itemStock.IncreaseItemCount(addX);
        assertEquals(itemStock.getCount(),result);

    }
    @ParameterizedTest(name = "Parametrizied test add {0} to count should equal {1}")
    @CsvSource({"12,-12","-3,3","0,0"})
    public void itemRemoveCountTest(int addX,int result)
    {
        Item item = new StandardItem(12,"Vysavač",47.67f,"Spotrebic",34);
        ItemStock itemStock = new ItemStock(item);
        itemStock.decreaseItemCount(addX);
        assertEquals(itemStock.getCount(),result);

    }

}